/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sudoku.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/09 21:02:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/04/09 21:45:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	"libft.h"

int	board[9][9];

void	ft_create_board(int c, int div)
{
	int	r;

	r = 8;
	while (div > 9)
	{
		board[c][r] = div % 10;
		div = div / 10;
		r--;
	}
	board[c][r] = div;
}

char	*ft_convert(char *str)
{
	char	*dest;
	int	i;

	i = 0;
	dest = (char*)malloc(sizeof(char) * 8);
	while (str[i])
	{
		if (str[i] == '.')
			dest[i] = '0';
		else
			dest[i] = str[i];
		i++;	
	}
	return (dest);
}

int	ft_check(int row, int col, int num)
{
	if (ft_check_row(row, board, num) != 1)
		return (0);
	if (ft_check_col(col, board, num) != 1)
		return (0);
	if (ft_check_small_box(row, col, board, num) != 1)
		return (0);
	return (1);
}

void	ft_solve_sudoku(int row, int col)
{
	int	num;

	if (col > 8)
		ft_print_sudoku(board);
	else if (row > 8)
		ft_solve_sudoku(0, col + 1);
	else if (board[col][row] != 0)
		ft_solve_sudoku(row + 1, col);
	else
	{
		num = 1;
		while (num <= 9)
		{
			if (ft_check(row, col, num) == 1)
			{
				board[col][row] = num;
				ft_solve_sudoku(row + 1, col);
			}
			board[col][row] = 0;
			num++;
		}
	}
}

int	main(int argc, char **argv)
{
	int	i;
	int	j;
	int	num;
	char	*dest;

	i = 1;
	j = 0;
	dest = (char*)malloc(sizeof(char) * 8);
	if (argc == 10)
	{
		while (i < argc)
		{
			dest = ft_convert(argv[i]);
			num = ft_atoi(dest);
			ft_create_board(j, num);
			free(dest);
			i++;
			j++;
		}
		ft_solve_sudoku(0, 0);
	}
	else
		ft_putstr("Error, eight rows needed.\n");
	return (0);
}

