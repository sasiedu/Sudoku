# ************************************************************************** #
#                                                                            #
#                                                        :::      ::::::::   #
#   Makefile                                           :+:      :+:    :+:   #
#                                                    +:+ +:+         +:+     #
#   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        #
#                                                +#+#+#+#+#+   +#+           #
#   Created: 2016/04/10 21:12:14 by sasiedu           #+#    #+#             #
#   Updated: 2016/04/10 21:25:54 by sasiedu          ###   ########.fr       #
#                                                                            #
# ************************************************************************** #

NAME = sudoku

SRC = tools.c checks.c main.c

BIN = tools.o checks.o main.o

HEADER = libft.h

all: $(NAME)

$(NAME):
	@gcc -Wall -Wextra -Werror -c $(HEADER) $(SRC)
	@gcc -Wall -Wextra -Werror $(BIN) -o $(NAME)

clean:
	@rm -f $(BIN)

fclean: clean
	@rm -f $(NAME)

re: fclean all
