/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/09 21:02:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/04/09 21:45:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef	LIBFT_H
# define LIBFT_H

# include	<unistd.h>
# include	<stdlib.h>

void	ft_create_board(int c, int div);
void	ft_solve_sudoku(int row, int col);
void	ft_putchar(char c);
void	ft_putstr(char *str);
void	ft_print_sudoku(int board[][9]);
char	*ft_convert(char *str);
int	ft_atoi(char *str);
int	ft_check(int row, int col, int num);
int	ft_check_row(int row, int board[][9], int num);
int	ft_check_col(int col, int board[][9], int num);
int	ft_check_small_box(int row, int col, int board[][9], int num);

#endif
